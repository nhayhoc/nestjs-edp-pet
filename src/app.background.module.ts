import { Module } from '@nestjs/common';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { UserBackgroundModule } from './user/user.background.module';

@Module({
  imports: [UserBackgroundModule, EventEmitterModule.forRoot()],
  controllers: [],
  providers: [],
})
export class AppBackgroundModule {}
