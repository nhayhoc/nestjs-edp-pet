import { NestFactory } from '@nestjs/core';
import { AppBackgroundModule } from './app.background.module';

async function bootstrap() {
  const app = await NestFactory.createApplicationContext(AppBackgroundModule);

  console.log('background running...');
}
bootstrap();
