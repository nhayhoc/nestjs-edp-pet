import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { UserQueueBackgroundService } from './background.queue.service';
import { BackgroundController } from './background.controller';

@Module({
  providers: [UserService, UserQueueBackgroundService],
  controllers: [UserController, BackgroundController],
})
export class UserModule {}
