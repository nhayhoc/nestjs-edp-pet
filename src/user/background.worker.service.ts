import { Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { Worker } from 'bullmq';
import { UserService } from './user.service';
import { EventEmitter2 } from '@nestjs/event-emitter';
@Injectable()
export class UserWorkerBackgroundService implements OnApplicationBootstrap {
  constructor(
    private readonly userService: UserService,
    private readonly eventEmitter: EventEmitter2,
  ) {}
  onApplicationBootstrap() {
    this.eventEmitter.on('test', (data) => {
      console.log('test event', data);
    });
    this.eventEmitter.emit('test', '1234');
    this.createSendGreetingsWorker();
  }

  createSendGreetingsWorker() {
    const worker = new Worker(
      'sendMail',
      async (job) => {
        console.log('Send email to ' + job.data.user.name);
      },
      {
        connection: {
          host: 'localhost',
          port: 6379,
        },
        concurrency: 1,
      },
    );
    worker.on('completed', (job) => {
      console.log(`Job completed with result ${job.returnvalue}`);
    });
    worker.on('failed', (job, err) => {
      console.log(`Job failed with reason ${err}`);
    });
  }
}
