import { Module } from '@nestjs/common';

import { UserWorkerBackgroundService } from './background.worker.service';
import { UserService } from './user.service';

@Module({
  providers: [UserService, UserWorkerBackgroundService],
  controllers: [],
})
export class UserBackgroundModule {}
